Birim ve ortalamalar
birim istatistik çalışmalarında hakkında bilgi sahibi olunmaya çalışılan yığın olay niteliğindeki ölçülebilir olay veya olguların her biri birimdir
Maddi ve maddi olmayan birim
maddi birim elle tutulur gözle görülebilir özellikte olan maddi varlığa sahip birimlerdir
maddi olmayan elle tutulamayan görülemeyen maddi olmayan birimlerdir
devamlı birim ani birim
bir defada olup bitmeyen ve istendiği zaman hakkında araştırma yapılabilecek birimlerdir ör motorlu taşıtkütüphane binası
devamlı birim devamlı mevcut olmayan belli şartlar olduğunda ölçülebilen sayılabilen birimlerdir
ör doğal afetler
gerçek birim varsayımsal birim
gerçek birim gerçekten olandır ör öğretmen
varsayımsal birim varsayımsal olandır ör  kişilik grubun içinden  er kişilik gruplardan her biri varsayımsal birimdir
geometrik ortalama gözlem sonuçlarının her biri bir önceki gözlem sonucuna bağlı olarak değişiyorsa ve bu değişmenin hızı saptanmak istenirse geometrik ortalama kullanılmalıdır kısaca G ile gösterilir veri değerlerinin pozitif olması gerekir
doğal birim doğal olmayan birim
parçalandığı yada birleştirildiğinde özelliğini kaybeden birim
ör öğrenci bilgisayar 
doğal olmayan birim 
parçalandığında özelliğini kaybeden birim ör tarla ekmek
harmonik ortalama ekonomik olaylarda  birim ile alınan ortalama miktara veya mamülün bir biriminin üretimi için harcanan ortalamaya gereksinim duyulduğunda kullanılır H ile gösterilir
iki veri için geçerli H 
A aritmetik ortalama ise
G  AH nin kareköküdür
varyans mümkün bütün değerlerin beklenen değer veya ortalamadan uzaklıklarının karelerinin ortalamasıdır
standart sapma varyansın kareköküdür
ölçüm birimine göre ya std yada varyans kullanılır eğer kg gibi bir ölçüm değerinin yayılımını ölçeceksek kilogramın karesini alamayacağımızdan std kullanmalıyız
standart moment ortalamaya göre momentin standart sapmaya bölünmesi
ortalamaya göre moment her elemanın ortalamadan farkının toplamının frekansların toplamına bölünmesidir
Karekök ortalama matematikte root mean square kısaltması RMS ya da rms ayrıca kuadratik ortalama olarak da bilinir Değişen miktarların büyüklüğünün ölçülmesinde kullanılan istatistik bir ölçüttür Değişimin artı ve eksi yönde olduğu dalgalarda özellikle çok faydalıdır
Sürekli olarak değişen bir fonksiyonun sürekli olmayan değer serisi için hesaplanabilir Karekök ortalama ismi karelerin ortalamasının karekökünün alınmasından gelir
standart sapma her elemanın karesinin her elemanın toplamının karesinin frekansların toplamına bölümünün farkıdır
çarpıklık
pearson ölçüsü
ortalamanın moddan farkının örnek standart sapmasına bölümüdür
simetrik serilerde sıfır sola çarpıklarlarda negatif sağa çarpıklarda pozitif çıkar
pearsın çarpıklık ölçüsü
ortalamanın medyandan farkının  katının örnek standart sapmaya bölünmesidir
aşırı asimetrik serilerde pearson çarpıklık ölçüsü kullanılır
Fri Dec    GMT GMT